﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication8
{
    public class ServiceChecker
    {
        private readonly string _url;
        private ApiResultDTO _result;

        public ServiceChecker(string url = "http://sses.status.ato.gov.au/api_get/currentStatus")
        {
            _url = url;
        }

        public bool FetchAndParseJson(string overrideJson = null)
        {
            try
            {
                string jsonToParse = overrideJson;
                if (overrideJson == null)
                {
                    jsonToParse = getDataFromURL(_url);
                }

                if (jsonToParse != null)
                {
                    _result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResultDTO>(jsonToParse);
                    return true;
                }
            }
            catch
            {
            }

            return false;
        }

        protected string getDataFromURL(string endpoint)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return streamReader.ReadToEnd();
                }
            }
            return null;
        }

        public bool IsServiceAvailable(EnumATOSystems systemName)
        {
            string nameInJson = null;

            if (systemName == EnumATOSystems.SuperMatch) nameInJson = "SuperMATCH 2";
            if (systemName == EnumATOSystems.SuperTick1) nameInJson = "SuperTICK 1";
            if (systemName == EnumATOSystems.SuperTick2) nameInJson = "SuperTICK 2";
            if (systemName == EnumATOSystems.SuperTick3) nameInJson = "SuperTICK 3";

            return IsServiceAvailable(nameInJson);
        }

        public bool IsServiceAvailable(string systemName)
        {
            foreach (var item in _result.rows)
            {
                if (item[0].Equals(systemName))
                {
                    if (item[1].Equals("Operational"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private class ApiResultDTO
        {
            public string[][] rows { get; set; }
        }
    }
}
