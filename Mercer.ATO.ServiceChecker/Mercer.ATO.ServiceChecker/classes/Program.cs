﻿using ConsoleApplication8;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication8
{
  
    class Program
    {
        static void Main(string[] args)
        {
            new Program().analyse();
        }

        void analyse()
        {
            ServiceChecker sc = new ServiceChecker();
            sc.FetchAndParseJson();

            bool superMatch2 = sc.IsServiceAvailable("SuperMATCH 2");
            bool superTick2 = sc.IsServiceAvailable("SuperTICK 2");
            
            Console.WriteLine(superMatch2);
            Console.WriteLine(superTick2);
            
            Console.ReadLine();
        }

      
    }

}
