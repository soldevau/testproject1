﻿using ConsoleApplication8;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    [TestFixture]
    public class Tests
    {
        string okJson = "{" + 
	        "\"preview\": false," +
            "\"init_offset\": 0," +
            "\"messages\": []," +
            "\"fields\": [\"Service_Name\", \"Status\", \"time\"]," + 
	        "\"rows\": [[\"SuperMATCH 2\", \"Operational\", \"1502847900\"], " + 
	         "[\"SuperTICK 1\", \"Operational\", \"1502847900\"], " + 
	        "[\"SuperTICK 2\", \"Down\", \"1502847900\"], " + 
            "[\"SuperTICK 3\", \"Operational\", \"1502847900\"]]}";

        [Test]
        public void Test_SuperMatch2Is_Up()
        {
            ServiceChecker sc = new ServiceChecker();
            sc.FetchAndParseJson(okJson);

            bool isSuperMatchOk = sc.IsServiceAvailable(EnumATOSystems.SuperMatch);
            Assert.IsTrue(isSuperMatchOk);
        }

        [Test]
        public void Test_SuperTick2_IsDown()
        {
            ServiceChecker sc = new ServiceChecker();
            sc.FetchAndParseJson(okJson);

            bool serviceOk = sc.IsServiceAvailable(EnumATOSystems.SuperTick2);
            Assert.IsFalse(serviceOk);
        }

        [Test]
        public void Test_SuperTick3_IsOk()
        {
            ServiceChecker sc = new ServiceChecker();
            bool success = sc.FetchAndParseJson(okJson);

            Assert.IsTrue(success);

            bool isServiceOk = sc.IsServiceAvailable("SuperTICK 3");

            Assert.IsTrue(isServiceOk);
        }

        [Test]
        public void Test_CheckService_InvalidJson()
        {
            ServiceChecker sc = new ServiceChecker();
            bool success = sc.FetchAndParseJson("this is invalid json");

            Assert.AreEqual(success, false);
            
        }


        [Test]
        public void Test_CheckService_NullJson()
        {
            ServiceChecker sc = new ServiceChecker();
            bool success = sc.FetchAndParseJson(null);

            Assert.AreEqual(success, false);
        }


    }
}
